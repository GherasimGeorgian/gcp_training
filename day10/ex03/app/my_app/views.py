import flask
from flask import request, send_file, Flask, render_template, make_response, jsonify
import requests
from my_app import my_app
from concurrent.futures import TimeoutError
from google.cloud import pubsub_v1


class PubSub:
    def __init__(self):
        self.list_messages=[]
    def run(self):
        # TODO(developer)
        project_id = "cosmic-zenith-327309"
        subscription_id = "Fully-automated-sub"
        # Number of seconds the subscriber should listen for messages
        timeout = 5.0

        subscriber = pubsub_v1.SubscriberClient()
        # The `subscription_path` method creates a fully qualified identifier
        # in the form `projects/{project_id}/subscriptions/{subscription_id}`
        subscription_path = subscriber.subscription_path(project_id, subscription_id)
        streaming_pull_future = subscriber.subscribe(subscription_path, callback=self.callback)
        print(f"Listening for messages on {subscription_path}..\n")

        # Wrap subscriber in a 'with' block to automatically call close() when done.
        with subscriber:
            try:
                # When `timeout` is not set, result() will block indefinitely,
                # unless an exception is encountered first.
                streaming_pull_future.result(timeout=timeout)
            except TimeoutError:
                streaming_pull_future.cancel()  # Trigger the shutdown.
                streaming_pull_future.result()  # Block until the shutdown is complete.

    def callback(self,message: pubsub_v1.subscriber.message.Message) -> None:
        print(f"Received {message}.")
        self.list_messages.append(message)
        message.ack()
    def get_messages(self):
        return self.list_messages

a=PubSub()

@my_app.route("/pull")
def pull_page():
    a.run()
    messages=[]
    for el in a.get_messages():
        dataw = el.data
        messages.append(dataw)
    return render_template("pull_page.html",messages=messages)
@my_app.route("/publish")
def publish_page():    
    return render_template("publish_page.html")


@my_app.route("/")
@my_app.route("/index.html")
def home_page():
    '''
    Afiseaza home page-ul
    :return: pagina web
    '''
    return render_template("index.html")

