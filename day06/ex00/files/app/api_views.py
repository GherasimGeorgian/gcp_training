from flask import jsonify,make_response,send_from_directory,send_file,render_template
from flask_restful import Api,Resource,reqparse
from flask_api import status
# Imports the Google Cloud client library
from google.cloud import datastore
from __init__ import documents_api
import requests
from fpdf import FPDF
import os
import csv
from urllib.request import urlopen

# Instantiates a client
datastore_client = datastore.Client()



item_post_args = reqparse.RequestParser()
item_post_args.add_argument("id",type=int,help="id of user",required=True)
item_post_args.add_argument("name",type=str,help="name of user",required=True)
item_post_args.add_argument("email",type=str,help="email of user",required=True)

BASE = "http://127.0.0.1:8080/"

class PDF(FPDF):

        def insert_text_blue(self,text,y):
            self.set_xy(0.0,y)
            self.set_font('Times', 'B', 16)
            self.set_text_color(0,0,255)
            self.cell(w=210.0, h=40.0, align='C', txt=text, border=0)

        def insert_text_green(self,text,y):
            self.set_xy(0.0,y)
            self.set_font('Times', 'B', 16)
            self.set_text_color(0,100,0)
            self.cell(w=210.0, h=40.0, align='C', txt=text, border=0)




@documents_api.route("/pdf/users", methods=['GET'],endpoint='get_pdf_users')
def get_pdf_users():
                response = requests.get(BASE + "user/list")
                pdf=PDF(orientation='L') 
                pdf.add_page()
                y=0
                for el in response.json():
                        pdf.insert_text_blue(el["email"],y)
                        pdf.insert_text_blue('\n',y)
                        y+=20
                pdf.output('users_pdf.pdf','F')
                return send_file("users_pdf.pdf", attachment_filename='users_pdf.pdf')

@documents_api.route("/pdf/secure/users", methods=['GET'],endpoint='get_pdf__secure_users')
def get_pdf__secure_users():
                r = requests.head('http://127.0.0.1:8080/user/list', data ={'key':'value'})
                
                size_list = int(r.headers.get('Content-Length'))
                if size_list < 5:
                        response = requests.get(BASE + "user/list")
                        pdf=PDF(orientation='L') 
                        pdf.add_page()
                        y=0
                        for el in response.json():
                                pdf.insert_text_green(el["email"],y)
                                pdf.insert_text_green('\n',y)
                                y+=20
                        pdf.output('users_pdf.pdf','F')
                        return send_file("users_pdf.pdf", attachment_filename='users_pdf.pdf')
                else:
                        raise Exception('error')


@documents_api.route("/csv/<min_id>/<max_id>", methods=['GET'],endpoint='get_users_csv')
def get_users_csv(min_id,max_id):
        response = requests.get(BASE + "user/list")
        
        with open('users_csv.csv', 'w', newline='') as file:
                writer = csv.writer(file)
                for el in response.json():
                        if ((el["id"] > int(min_id)) and (el["id"] < int(max_id))):
                                writer.writerow([el["name"]])
        return send_file("users_csv.csv", attachment_filename='users_csv.csv')

@documents_api.route("/html/another_page",methods=['GET'],endpoint='get_another_page')
def get_another_page():
        try:
                url = "https://developer.mozilla.org/en-US/docs/Web/HTTP."
                html = urlopen(url).read()
                hs = open("content.html", 'wb')
                hs.write(html)
                return send_file("content.html", attachment_filename='content.html'),200
        except:
                return "Error to get data",404

@documents_api.route("/author",methods=['GET'],endpoint='get_image')
def get_image():
        return send_file("./app/static/img/cat.jpg", attachment_filename='author.jpg'),200
