import requests


BASE = "http://127.0.0.1:5000/"

requests.post(BASE + "user/add" ,{"id":1,"name":"ionut","email":"ion@gmail.com"})

response = requests.get(BASE + "user/list")

print("Response list:", response.json())

response = requests.get(BASE + "user/1")

print("User returned:",response.json())

requests.put(BASE + "user/3" ,{"name":"ionut4","email":"ion3@gmail.com"})

requests.patch(BASE + "user/3",{"email":"newemail@gmail.com"})

requests.delete(BASE + "user/3")


