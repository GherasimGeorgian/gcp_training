from flask import render_template,make_response,request
from __init__ import routes


@routes.route('/')
def homePage():
    res1 = make_response(render_template("light.html"))
    res2 = make_response(render_template("dark.html"))
    cookies = request.cookies
    mode = cookies.get("mode")
    if mode == "light":
        return res1
    else:
        return res2


@routes.route("/dark/", methods=['POST'])
def dark_mode():
    res1 = make_response(render_template("dark.html"))
    res1.set_cookie("mode","dark")
    return res1


@routes.route("/light/", methods=['POST'])
def light_mode():
    res1 = make_response(render_template("light.html"))
    res1.set_cookie("mode","light")
    return res1


@routes.route('/contact')
def contactPage():
    return render_template("contact.html")
@routes.route('/prices')
def pricesPage():
    return render_template("prices.html")