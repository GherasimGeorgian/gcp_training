what are the differences
between database and datawarehouse

A database is any collection of data organized for storage, accessibility, and retrieval. A data warehouse is a type of database the integrates copies of transaction data from disparate source systems and provisions them for analytical use.

a = Database
b = datawarehouse

1 - a - Operational systems are designed to support high-volume transaction processing.
    b - Data warehousing systems are typically designed to support high-volume analytical processing (i.e., OLAP).
2 - a - Operational systems are usually concerned with current data.
    b - Data warehousing systems are usually concerned with historical data.
3 - a - Data within operational systems are mainly updated regularly according to need.
    b - Non-volatile, new data may be added regularly. Once Added rarely changed.
4 - a - It is designed for real-time business dealing and processes.
    b - It is designed for analysis of business measures by subject area, categories, and attributes.
5 - a - It is optimized for a simple set of transactions, generally adding or retrieving a single row at a time per table.
    b - It is optimized for extent loads and high, complex, unpredictable queries that access many rows per table.
6 - a - It is optimized for validation of incoming information during transactions, uses validation data tables.
    b - Loaded with consistent, valid information, requires no real-time validation.
7 - a - It supports thousands of concurrent clients.
    b - It supports a few concurrent clients relative to OLTP.
8 - a - Operational systems are widely process-oriented.
    b - Data warehousing systems are widely subject-oriented
9 - a - Operational systems are usually optimized to perform fast inserts and updates of associatively small volumes of data.
    b - Data warehousing systems are usually optimized to perform fast retrievals of relatively high volumes of data.
10- a - Data In
    b - Data out
11- a - Less Number of data accessed.
    b - Large Number of data accessed.
12- a - Relational databases are created for on-line transactional Processing (OLTP)
    b - Data Warehouse designed for on-line Analytical Processing (OLAP)

