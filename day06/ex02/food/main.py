from functools import cache
from flask import Flask
import sys
sys.path.append("app")
from api_views import food_api
from views import routes
from flask_caching import Cache

cache = Cache()

app = Flask(__name__,template_folder='app/templates/public',static_folder="app/static/img")
app.register_blueprint(food_api)
app.register_blueprint(routes)

app.config['CACHE_TYPE'] = 'simple'
cache.init_app(app)

if __name__=="__main__":
    app.run(host='127.0.0.1', port=5000)