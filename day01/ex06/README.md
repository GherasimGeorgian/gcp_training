In simple terms, a stack trace is a list of the method calls that the application was in the middle of when an Exception was thrown.
