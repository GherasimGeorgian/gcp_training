import json
class Product():
    def __init__(self, id,name,price):
        self._id = id
        self._name= name
        self._price = price
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)   
    def __str__(self):
        return str(self._id) + "-" + str(self._name) + "-" + str(self._price)  
    def getId(self):
        return self._id
    def setName(self,name):
        self._name=name
    def setPrice(self,price):
        self._price=price