from flask import Flask,jsonify,abort,render_template
import sys
sys.path.append("app")
from api_views import product_api
from views import routes
from werkzeug import exceptions
from werkzeug.exceptions import HTTPException
from functools import wraps
app = Flask(__name__,template_folder='app/templates/public',static_folder="app/static/img")
app.register_blueprint(product_api)
app.register_blueprint(routes)

@app.errorhandler(HTTPException)
def handle_error(error):
    print("-----",error)
    code = 500
    if isinstance(error, HTTPException):
        code = error.code
    return render_template("internal_se.html")

for cls in HTTPException.__subclasses__():
    app.register_error_handler(cls, handle_error)




if __name__=="__main__":
    app.run(host='127.0.0.1', port=5000)