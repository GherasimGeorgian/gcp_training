from google.cloud import bigquery
import csv
import json

def query_stackoverflow():
    client = bigquery.Client()
    query_job = client.query(
        "SELECT A.display_name,B.score FROM `bigquery-public-data.stackoverflow.comments` B  join `bigquery-public-data.stackoverflow.users` A on A.id = B.user_id group by A.display_name,B.score order by B.score desc LIMIT 10"
        )

    results = query_job.result()  # Waits for job to complete.
    dictionary = {}
    
    for row in results:
            dictionary[row.display_name]=row.score
    with open('result.json','w') as f:
        f.write(json.dumps(dictionary))
if __name__ == "__main__":
    query_stackoverflow()


