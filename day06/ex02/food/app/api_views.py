from datetime import datetime, timedelta
from flask import jsonify,make_response,send_from_directory,send_file,render_template
from __init__ import food_api
import requests
from PIL import Image
from main import cache
last_call_cached_image = datetime.now()

@food_api.route("/random_simple",methods=['GET'],endpoint='random_simple')
def random_simple():
        img = requests.get("https://foodish-api.herokuapp.com/api/")
        
        print(img.json()['image'])
        response = requests.get(img.json()['image'])

        file = open("./app/static/img/sample_image.jpg", "wb")
        file.write(response.content)
        file.close()
        return send_file("./app/static/img/sample_image.jpg", attachment_filename='sample_image.jpg'),200


@food_api.route("/random_cached",methods=['GET'],endpoint='random_cached')
def random_cached():
        global last_call_cached_image
        if last_call_cached_image < datetime.now()-timedelta(seconds=5):
                generate_image()
        return send_file("./app/static/img/cached_image.jpg", attachment_filename='cached_image.jpg'),200

@cache.cached(timeout=5)
def generate_image():
        global last_call_cached_image
        last_call_cached_image = datetime.now()
        img = requests.get("https://foodish-api.herokuapp.com/api/")
        print(img.json()['image'])
        response = requests.get(img.json()['image'])        
        file = open("./app/static/img/cached_image.jpg", "wb")
        file.write(response.content)
        file.close()



# @food_api.route("/random_cached",methods=['GET'],endpoint='random_cached')
# def random_cached():
#         global last_call_cached_image
#         if last_call_cached_image < datetime.now()-timedelta(seconds=5):
#                 last_call_cached_image = datetime.now()
#                 img = requests.get("https://foodish-api.herokuapp.com/api/")
        
#                 print(img.json()['image'])
#                 response = requests.get(img.json()['image'])

#                 file = open("./app/static/img/cached_image.jpg", "wb")
#                 file.write(response.content)
#                 file.close()
#         return send_file("./app/static/img/cached_image.jpg", attachment_filename='cached_image.jpg'),200

