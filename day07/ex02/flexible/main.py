from flask import Flask
import sys
sys.path.append("app")
from api_views import user_api
from views import routes

app = Flask(__name__,template_folder='app/templates/public',static_folder="app/static/img")
app.register_blueprint(user_api)
app.register_blueprint(routes)

if __name__=="__main__":
    app.run(host='127.0.0.1', port=8080)
