Google Cloud Datastore vs. MySQL

As for speed, NoSQL is generally faster than SQL, especially for key-value storage in our experiment; On the other hand, NoSQL database may not fully support ACID transactions, which may result data inconsistency.
SQL databases are table based in the form of row & columns and must strictly adhere to standard schema definitions.
They are a better option for applications which need multi-row transactions.
NoSQL databases can be based on documents, key-value pairs, graphs or columns and they don’t have to stick to standard schema definitions.
Sql databases  have a well-designed pre-defined schema for structured data.
No sql databases  have the dynamic schema for unstructured data. Data can be flexibly stored without having a pre-defined structure.
