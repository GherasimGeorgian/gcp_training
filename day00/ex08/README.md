TLS(Transport Layer Security) sunt protocoale care permit o comunicare sigura pe internet.O utilizare a protocolului TLS este criptarea comunicarii dintre aplicatiile web si servere,cum ar fi browserele web.De aseamanea el mai poate fi folosit pentru a cipta si alte comunicatii cum ar fi email,messaging si VoIP. Exista 3 componente principale pe care le realizeaza:Encryption,Authentication si Integrity.

Pentru ca un site web sau o aplicație să utilizeze TLS, acesta trebuie sa aiba un certificat TLS instalat pe serverul sau de origine (certificatul este, de asemenea, cunoscut sub numele de certificat SSL din cauza confuziei de denumire descrise mai sus). Un certificat TLS este emis de o autoritate de certificare persoanei sau companiei care detine un domeniu. Certificatul contine informatii importante despre cine detine domeniul, împreună cu cheia publica a serverului, ambele fiind importante pentru validarea identitatii serverului.

TLS handshake are loc ori de cate ori un utilizator navigheaza catre un site web prin HTTPS si browserul incepe mai intai să interogheze serverul de origine al site-ului.
-are loc, de asemenea, ori de cate ori orice alta comunicatie utilizeaza HTTPS, inclusiv apeluri API și interogari DNS peste HTTPS.

TLS handshakes - apar dupa ce o conexiune TCP a fost deschisa printr-o strangere de mana TCP.


