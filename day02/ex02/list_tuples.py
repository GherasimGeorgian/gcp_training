import random
#Write a python function that generates a list containing tuples. The
#list receives two parameters a and b and must return a list
#containing tuples with the second element (x) a number between a
#and b in ascending order and the first element a number between 0
#and x in descending order.
#• If not considering the return statement and the header of the
#function, the code must have a maximum of one line of code. Use
#list comprehension.
def gen_tuples(start,end):
    tuples = [(end-i, i+start)  for i in range(0,end-start) ]
    
    print(tuples)
