from flask import render_template
from __init__ import routes
from flask import request
import requests

@routes.route('/user_agent')
def user_agent():
    return "Details about the requesting agent are:" + request.headers.get('User-Agent')

@routes.route('/host')
def host():
    return "The Ip address of the requesting agent is:" + str(request.environ['REMOTE_ADDR']) + " and the port is " + str(request.environ['REMOTE_PORT'])

@routes.route('/accepted-files')
def accepted_files():
    return "The accepted response content types are: " + str(request.headers.get('Accept'))


@routes.route('/')
def homePage():
    return render_template("homepage.html")
@routes.route('/contact')
def contactPage():
    return render_template("contact.html")
@routes.route('/prices')
def pricesPage():
    return render_template("prices.html")