from flask import Flask,render_template
from datetime import datetime

import mysql.connector
from mysql.connector.constants import ClientFlag

config = {
    'user': 'root',
     'password': '1234',
     'host': '127.0.0.1',
    'client_flags': [ClientFlag.SSL],
    'ssl_ca': 'ssl/server-ca.pem',
    'ssl_cert': 'ssl/client-cert.pem',
    'ssl_key': 'ssl/client-key.pem'    
}

# now we establish our connection
cnxn = mysql.connector.connect(**config)

cursor = cnxn.cursor()  # initialize connection cursor
#cursor.execute('CREATE DATABASE timestamps')  # create a new 'testdb' database
#cnxn.close()

config['database'] = 'timestamps'  # add new database to config dict
cnxn = mysql.connector.connect(**config)
cursor = cnxn.cursor()



#cursor.execute("CREATE TABLE ts_table("
#              "last_dt varchar(255))")

#cnxn.commit()  # this commits changes to the database



app = Flask(__name__,template_folder='app/templates/public')

@app.route("/")
def hello_world():
    # first we setup our query
    query = ("INSERT INTO ts_table (last_dt) "
         "VALUES (%s)")
    date_c = datetime.now()
    date_time = date_c.strftime("%m/%d/%Y, %H:%M:%S")
    
    
    cursor.execute(query,(date_time,))
    cnxn.commit()  # and commit changes
    

    cursor.execute("SELECT * FROM ts_table limit 20")
    out = cursor.fetchall()
    

    return render_template("index.html",result = out)

if __name__=="__main__":
    app.run(host='127.0.0.1', port=5000)

