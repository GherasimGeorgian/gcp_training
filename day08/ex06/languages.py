from google.cloud import bigquery
import csv


def query_stackoverflow():
    client = bigquery.Client()
    tag="\"python\""
    query_job = client.query(
        "SELECT title as post_title,body,tags as all_tags FROM `bigquery-public-data.stackoverflow.stackoverflow_posts` where CONTAINS_SUBSTR(tags,"+tag+")"
        )

    results = query_job.result()  # Waits for job to complete.
    header_added=False
    with open('result.csv', 'w') as f:
                for row in results:
                    if not header_added:
                        f.write(f'post_title,body,all_tags\n')
                        header_added=True
                    f.write(f'{row.post_title},{row.body},{row.all_tags}\n')

if __name__ == "__main__":
    query_stackoverflow()


