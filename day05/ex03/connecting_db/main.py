from flask import Flask
import sys
sys.path.append("app")
from api_views import item_api
from views import routes

app = Flask(__name__,template_folder='app/templates/public',static_folder="app/static/img")
app.register_blueprint(item_api)
app.register_blueprint(routes)

if __name__=="__main__":
    app.run(debug=True)