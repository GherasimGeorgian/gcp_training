import threading
import time
def thread_function(dur):
    time.sleep(60*dur)


if __name__ == "__main__":
    threads = list()

    for index in range(1,6):
        x = threading.Thread(target=thread_function, args=(index,))
        threads.append(x)
        x.start()
    for index, thread in enumerate(threads):
        thread.join()
