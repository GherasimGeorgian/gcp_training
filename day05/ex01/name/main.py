from flask import Flask, render_template
app = Flask(__name__,template_folder='app/templates/public',static_folder="app/static/img")
@app.route('/hello_to_training/<name>')
def greet(name):
    return render_template("index.html",nameTxt=name)

if __name__=="__main__":
    app.run(host='127.0.0.1', port=8080)