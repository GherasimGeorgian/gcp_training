from google.cloud import bigquery
import csv
from fpdf import FPDF

class PDF(FPDF):

        def insert_text_title(self,text,y):
            self.set_xy(0.0,y)
            self.set_font('Arial', 'B', 24)
            self.set_text_color(0,100,0)
            self.cell(w=210.0, h=40.0, align='C', txt=text.upper(), border=0)

        def insert_text_body(self,text,y):
            self.set_xy(0.0,y)
            self.set_font('Arial', 'B', 12)
            self.set_text_color(0,0,255)
            self.cell(w=210.0, h=40.0, align='L', txt=text[0:100], border=0)
        


def query_stackoverflow():
    client = bigquery.Client()
    
    query_job = client.query(
        "SELECT title,body,DATE_DIFF(DATE(last_activity_date), DATE(creation_date), DAY) as lifetime FROM `bigquery-public-data.stackoverflow.stackoverflow_posts`order by answer_count desc LIMIT 15"
        )


    results = query_job.result()  # Waits for job to complete.
    pdf=PDF(orientation='P') 
    pdf.add_page()
    y=0
    for row in results:
        pdf.insert_text_title(row.title,y)
        pdf.insert_text_title('\n',y)
        y+=20
        pdf.insert_text_body(row.body,y)
        pdf.insert_text_title('\n',y)
        y+=20
    pdf.output('result.pdf','F')
if __name__ == "__main__":
    query_stackoverflow()

