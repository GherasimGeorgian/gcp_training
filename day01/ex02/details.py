from datetime import datetime
import sys
def get_details():
    print("-Python version ",sys.version)
    # datetime object containing current date and time
    now = datetime.now()
    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    print("-Current date and time:", dt_string)
