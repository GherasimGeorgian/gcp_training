import psutil
import os
for pid_id in psutil.pids():
    try:
        
        process_pid = psutil.Process(pid_id)
        with process_pid.oneshot():
            print ("Proccess id:{:<5} | Status: {:<10} | Priority {:<5} | CPU %: {:<5} | Username: {:<10} | Parent id: {:<5} | Current working dir: {:<10}".format(pid_id,process_pid.status(),process_pid.nice(),process_pid.cpu_percent(),process_pid.username(),process_pid.ppid(),process_pid.exe()))

    except Exception:
        print("Error print id",pid_id)
print(psutil.pids())
