from flask import jsonify,make_response,redirect,url_for
from flask_restful import Api,Resource,reqparse
from flask_api import status
# Imports the Google Cloud client library
from google.cloud import datastore
from __init__ import item_api



# Instantiates a client
datastore_client = datastore.Client()



item_post_args = reqparse.RequestParser()
item_post_args.add_argument("id",type=int,help="id of user",required=True)
item_post_args.add_argument("name",type=str,help="name of user",required=True)
item_post_args.add_argument("email",type=str,help="email of user",required=True)

@item_api.route("/user/list", methods=['HEAD'],endpoint='get_size_list')
def get_size_list():
        string_elements=""
        try:
                query = datastore_client.query(kind="user")
                results = list(query.fetch())
                for el in results:
                        string_elements+="0"
                return string_elements
        except:
                raise Exception("error get list")  


@item_api.route("/user/list", methods=['GET'],endpoint='get_list')
def get_list():
        try:
                query = datastore_client.query(kind="user")
                results = list(query.fetch())
                json_list = []
                for el in results:
                        json_list.append(dict(el))
                return jsonify(json_list)
        except:
                raise Exception("error get list") 
        

@item_api.route("/user/add", methods=['POST'], endpoint='save_user')
def save_item():
        try:
                args = item_post_args.parse_args()
                id = int(args['id'])
                name = args['name']
                email = args['email']
                #print(id+ "  " + name + "  " + email)
                # The kind for the new entity
                kind = "user"
                # The name/ID for the new entity
                # The Cloud Datastore key for the new entity
                item_key = datastore_client.key(kind, id)
                # Prepares the new entity
                item = datastore.Entity(key=item_key)
                item["id"] = id
                item["name"] = name
                item["email"] = email
                # Saves the entity
                datastore_client.put(item)
        except: 
                return "Error",status.HTTP_404_NOT_FOUND
        return "OK",status.HTTP_200_OK
       
        

@item_api.route("/user/<string:iduser>", methods=['GET'], endpoint='get_user')
def get_user(iduser):
        query = datastore_client.query(kind="user")
        query_iter = query.fetch()
        for entity in query_iter:
                if (int(entity.key.id) == int(iduser)):
                        return (dict(entity))

item_post_args2 = reqparse.RequestParser()
item_post_args2.add_argument("name",type=str,help="name of user",required=True)
item_post_args2.add_argument("email",type=str,help="email of user",required=True)

@item_api.route("/user/<int:iduser>", methods=['PUT'],endpoint='update_user')
def update_user(iduser):
        try:
                args = item_post_args2.parse_args()
                name = args['name']
                email = args['email']
                print(str(iduser) + " " + name + "    " + email)
                query = datastore_client.query(kind="user")
                query_iter = query.fetch()
                gasit=None
                for entity in query_iter:
                        if (int(entity.key.id) == int(iduser)):
                                gasit = entity
                if gasit != None:
                        gasit['name'] = name
                        gasit['email'] = email
                        datastore_client.put(gasit)
                        return "OK",status.HTTP_200_OK
                else:
                        kind = "user"
                        # The name/ID for the new entity
                        # The Cloud Datastore key for the new entity
                        item_key = datastore_client.key(kind, iduser)
                        # Prepares the new entity
                        item = datastore.Entity(key=item_key)
                        item["id"] = iduser
                        item["name"] = name
                        item["email"] = email
                        # Saves the entity
                        datastore_client.put(item)
                        return "OK",status.HTTP_200_OK
        except: return "Error",status.HTTP_404_NOT_FOUND


item_post_args_email = reqparse.RequestParser()
item_post_args_email.add_argument("email",type=str,help="email of user",required=True)

@item_api.route("/user/<int:iduser>", methods=['PATCH'],endpoint='update_user_email')
def update_user_email(iduser):
        try:
                args = item_post_args_email.parse_args()
                email = args['email']
                query = datastore_client.query(kind="user")
                query_iter = query.fetch()
                gasit=None
                for entity in query_iter:
                        if (int(entity.key.id) == int(iduser)):
                                gasit = entity
                if gasit != None:
                        gasit['email'] = email
                        datastore_client.put(gasit)
                else:
                        return "Error",status.HTTP_404_NOT_FOUND
                return "OK",status.HTTP_200_OK
        except: return "Error",status.HTTP_404_NOT_FOUND

@item_api.route("/user/<int:iduser>", methods=['DELETE'],endpoint='delete_user')
def delete_user(iduser):
        try:
                query = datastore_client.query(kind="user")
                query_iter = query.fetch()
                gasit=None
                for entity in query_iter:
                        if (int(entity.key.id) == int(iduser)):
                                gasit = entity
                if gasit != None:
                        datastore_client.delete(gasit.key)
                else:
                        return "Error",status.HTTP_404_NOT_FOUND
                return "OK",status.HTTP_200_OK
        except: return "Error",status.HTTP_404_NOT_FOUND
