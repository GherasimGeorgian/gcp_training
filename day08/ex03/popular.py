from google.cloud import bigquery
import csv


def query_stackoverflow():
    client = bigquery.Client()
    query_job = client.query(
        "SELECT A.display_name,count(B.id) as nr_of_comments FROM `bigquery-public-data.stackoverflow.comments` B  join `bigquery-public-data.stackoverflow.users` A on A.id = B.user_id group by A.display_name order by nr_of_comments desc LIMIT 10"
        
        )

    results = query_job.result()  # Waits for job to complete.
    
    with open('result.txt', 'w') as f:
        for row in results:
            f.write('['+row.display_name+'] -> ['+str(row.nr_of_comments)+']\n')

if __name__ == "__main__":
    query_stackoverflow()

