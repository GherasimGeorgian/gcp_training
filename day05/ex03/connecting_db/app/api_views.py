from flask import jsonify,make_response
from flask_restful import Api,Resource,reqparse
# Imports the Google Cloud client library
from google.cloud import datastore
from __init__ import item_api



# Instantiates a client
datastore_client = datastore.Client()



item_post_args = reqparse.RequestParser()
item_post_args.add_argument("name",type=str,help="name of item",required=True)
item_post_args.add_argument("price",type=int,help="price of item",required=True)


@item_api.route("/api/list", methods=['GET'],endpoint='get_list')
def get_list():
        query = datastore_client.query(kind="item")
        results = list(query.fetch())
        json_list = []
        for el in results:
            json_list.append(dict(results[0]))
        return jsonify(json_list)

@item_api.route("/api/add", methods=['POST'], endpoint='save_item')
def save_item():
        args = item_post_args.parse_args()
        name = args['name']
        price = int(args['price'])
        # The kind for the new entity
        kind = "item"
        # The name/ID for the new entity
        # The Cloud Datastore key for the new entity
        item_key = datastore_client.key(kind, name)
        # Prepares the new entity
        item = datastore.Entity(key=item_key)
        item["name"] = name
        item["price"] = price
        # Saves the entity
        datastore_client.put(item)
        print(f"Saved {item.key.name}: {item['name']}")
        return make_response(jsonify(item), 200)