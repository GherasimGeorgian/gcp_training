def show_intervals():
    numbers_int = input("type here the ends of the range:")
    if(len(numbers_int)==0):
        raise EOFError("No input privided!")
    numbers =  numbers_int.split()
    if(len(numbers) != 2):
        raise ValueError('Enter only 2 values!')
    first_number=0
    second_number=0
    try:
        first_number = int(numbers[0])
        second_number= int(numbers[1])
    except ValueError:
        raise TypeError("Incorect type!")
    if(first_number>second_number):
        raise ValueError("Invalid interval!")
    
    for i in range(first_number,second_number+1):
        print(i)

    
