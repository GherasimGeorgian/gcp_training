from os import abort
from flask import jsonify,make_response,send_from_directory,send_file,render_template
from domain.Product import Product
from __init__ import product_api
from flask_restful import reqparse

list_of_products = []
p1 = Product(1,"telefon",12)
p2 = Product(2,"carte",3)
p3 = Product(3,"cutit",7)
last_id = 4

list_of_products.append(p1)
list_of_products.append(p2)
list_of_products.append(p3)

@product_api.route("/error/internal_server_error",methods=['GET'],endpoint='internal_server_error')
def internal_server_error():
       abort(500)
       
@product_api.route("/error/permanent_redirect",methods=['GET'],endpoint='permanent_redirect')
def permanent_redirect():
        abort(460)

@product_api.route("/error/service_unaivailable",methods=['GET'],endpoint='service_unaivailable')
def service_unaivailable():
        abort(503)

@product_api.route("/error/non_standard_insufficient_storage",methods=['GET'],endpoint='non_standard_insufficient_storage')
def non_standard_insufficient_storage():
        abort(507)




@product_api.route("/list",methods=['GET'],endpoint='get_list_products')
def get_list_products():
        json_list = []
        for el in list_of_products:
            json_list.append(el.toJSON())
        return jsonify(json_list),200


product_post_args = reqparse.RequestParser()
product_post_args.add_argument("name",type=str,help="name of product",required=True)
product_post_args.add_argument("price",type=int,help="price of product",required=True)


@product_api.route("/add",methods=['POST'],endpoint='save_product')
def save_product():
        args = product_post_args.parse_args()
        name = args['name']
        price = int(args['price'])
        global last_id
         
        p = Product(last_id,name,price)
        last_id += 1
        list_of_products.append(p)

        for el in list_of_products:
                print("Element :",str(el))
        
        return "OK",200
@product_api.route("/find/<iduser>",methods=['GET'],endpoint='findUser')
def findUser(iduser):
        for el in list_of_products:
                if el.getId() == int(iduser):
                        return el.toJSON()
        return "Product doesn't exist",404

@product_api.route("/modify/<iduser>",methods=['PUT'],endpoint='modify_user')
def modify_user(iduser):
        args = product_post_args.parse_args()
        name = args['name']
        price = int(args['price'])
        ok=0
        for el in list_of_products:
                if int(el.getId()) == int(iduser):
                        el.setName(name)
                        el.setPrice(price) 
                        ok=1
                        return "OK",200
        if ok == 0:
                
                p = Product(iduser,name,price)
                list_of_products.append(p)

                for el in list_of_products:
                        print("Element :",str(el))
                
        return "OK",200

product_price_args = reqparse.RequestParser()
product_price_args.add_argument("price",type=int,help="price of product",required=True)

@product_api.route("/partial_modify/<iduser>",methods=['PATCH'],endpoint='modify_email')
def modify_email(iduser):
        args = product_price_args.parse_args()
        price = int(args['price'])
        ok=0
        for el in list_of_products:
                if int(el.getId()) == int(iduser):
                        el.setPrice(price) 
                        ok=1
        for el in list_of_products:
                        print("Element :",str(el))
        if ok == 1:                
                return "OK",200
        if ok == 0:
                return "Product doesn't exist",404  
@product_api.route("/delete/<iduser>",methods=['DELETE'],endpoint='delete_product')
def delete_product(iduser):
        ok=0
        for el in list_of_products:
                if int(el.getId()) == int(iduser):
                        list_of_products.remove(el) 
                        ok=1
        for el in list_of_products:
                        print("Element :",str(el))   
        if ok == 1:                
                return "OK",200
        if ok == 0:
                return "Product doesn't exist",404       

