from google.cloud import bigquery
import csv
import json

def query_stackoverflow():
    com="\".com\""
    client = bigquery.Client()
    query_job = client.query(
        "SELECT display_name,up_votes,website_url as domain from `bigquery-public-data.stackoverflow.users` where CONTAINS_SUBSTR(website_url,"+com+") order by up_votes desc LIMIT 15"        
        )

    results = query_job.result()  # Waits for job to complete.
    list_elems=[]
    for row in results:
        list_elems.append({row.display_name:[row.up_votes,row.domain]})
    with open('result.json', 'w') as f:
        f.write(json.dumps(list_elems))

if __name__ == "__main__":
    query_stackoverflow()

