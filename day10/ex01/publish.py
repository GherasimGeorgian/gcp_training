from google.cloud import pubsub_v1
import time
# TODO(developer)
project_id = "cosmic-zenith-327309"
topic_id = "starting-with-UI"

publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(project_id, topic_id)

for n in range(1, 6):
    data = "This is sent from code message id:"+str(n)
    # Data must be a bytestring
    data = data.encode("utf-8")
    # Add two attributes, origin and username, to the message
    future = publisher.publish(
        topic_path, data, origin="python-sample", username="gcp"
    )
    print(future.result())

