from datetime import datetime, timedelta
from flask import jsonify,make_response,send_from_directory,send_file,render_template
from __init__ import user_api
import requests
from PIL import Image


@user_api.route("/random_simple",methods=['GET'],endpoint='random_simple')
def random_simple():
        img = requests.get("https://foodish-api.herokuapp.com/api/")
        
        print(img.json()['image'])
        response = requests.get(img.json()['image'])

        file = open("./app/static/img/sample_image.jpg", "wb")
        file.write(response.content)
        file.close()
        return send_file("./app/static/img/sample_image.jpg", attachment_filename='sample_image.jpg'),200


