from google.cloud import bigquery
import csv
import json
from google.cloud import datastore
def query_stackoverflow():
    client = bigquery.Client()
    # Instantiates a client
    datastore_client = datastore.Client()

    query_job = client.query("SELECT title,body,DATE_DIFF(DATE(last_activity_date), DATE(creation_date), DAY) as lifetime FROM `bigquery-public-data.stackoverflow.stackoverflow_posts` order by lifetime desc LIMIT 15")

    results = query_job.result()  # Waits for job to complete.
    
    for row in results:
                # The kind for the new entity
                kind = "lifetime"
                # The name/ID for the new entity
                # The Cloud Datastore key for the new entity
                item_key = datastore_client.key(kind,str(row.title))
                # Prepares the new entity
                item = datastore.Entity(key=item_key)
                item["title"] = row.title
                item["body"] = row.body[0:100]
                item["lifetime"] = row.lifetime 
                # Saves the entity
                datastore_client.put(item)
                
if __name__ == "__main__":
    query_stackoverflow()

