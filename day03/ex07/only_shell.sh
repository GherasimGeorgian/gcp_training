gcloud compute networks create net-03 --project=infra-327611 --subnet-mode=custom --mtu=1460 --bgp-routing-mode=regional

gcloud compute networks subnets create subnet1 --project=infra-327611 --range=10.0.0.0/9 --network=net-03 --region=us-central1

gcloud compute networks subnets create subnet2 --project=infra-327611 --range=11.0.0.0/9 --network=net-03 --region=europe-central2

gcloud compute instances create my-vm-us2 --project=infra-327611 --zone=us-central1-a --machine-type=e2-medium --network-interface=network-tier=PREMIUM,subnet=subnet1 --maintenance-policy=MIGRATE --service-account=749208593888-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --create-disk=auto-delete=yes,boot=yes,device-name=my-vm-us2,image=projects/debian-cloud/global/images/debian-10-buster-v20210916,mode=rw,size=10,type=projects/infra-327611/zones/us-central1-a/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any

#I GIVE UP - us-central-2 doesnt exist
