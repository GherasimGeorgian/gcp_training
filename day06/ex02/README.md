HTTP caching
The performance of web sites and applications can be significantly improved by reusing previously fetched resources. Web caches reduce latency and network traffic and thus lessen the time needed to display resource representations. HTTP caching makes Web sites more responsive.

Types of caches
Caching is a technique that stores a copy of a given resource and serves it back when requested. When a web cache has a requested resource in its store, it intercepts the request and returns a copy of the stored resource instead of redownloading the resource from the originating server. This achieves several goals: it eases the load of the server because it doesn’t need to serve all clients itself, and it improves performance by being closer to the client. In other words, it takes less time to transmit the resource back. For a web site, web caching is a major component in achieving high performance. However, the cache functionality must be configured properly, as not all resources stay identical forever: it's important to cache a resource only until it changes, not longer.



The main difference between Cache and Cookie is that, Cache is used to store online page resources during a browser for the long run purpose or to decrease the loading time. On the other hand, cookies are employed to store user choices such as browsing session to trace the user preferences.


The Cache-Control HTTP header holds directives (instructions) for caching in both requests and responses. If a given directive is in a request, it does not mean this directive is in the response.

Caching directives follow the validation rules below:

Case-insensitive, but lowercase is recommended.
Multiple directives are comma-separated.
Some directives have an optional argument, which can be either a token or a quoted-string. (See spec for definitions)
Cache request directives
Standard Cache-Control directives that the client can use in an HTTP request:

Cache-Control: max-age=<seconds>
Cache-Control: max-stale[=<seconds>]
Cache-Control: min-fresh=<seconds>
Cache-Control: no-cache
Cache-Control: no-store
Cache-Control: no-transform
Cache-Control: only-if-cached