from google.cloud import bigquery
import csv


def query_stackoverflow():
    client = bigquery.Client()
    query_job = client.query(
        "SELECT A.display_name,B.score FROM `bigquery-public-data.stackoverflow.comments` B  join `bigquery-public-data.stackoverflow.users` A on A.id = B.user_id order by B.score desc LIMIT 10"
    )

    results = query_job.result()  # Waits for job to complete.
    header_added=False
    with open('result.csv', 'w') as f:
                for row in results:
                    if not header_added:
                        f.write(f'user_display_name,highest_score\n')
                        header_added=True
                    f.write(f'{row.display_name},{row.score}\n')

if __name__ == "__main__":
    query_stackoverflow()

