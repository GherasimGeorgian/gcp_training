import time
from random import randint
def stress():
    nr_of_nrs_generated=0
    
    while(nr_of_nrs_generated<10):
        value_generate = randint(0, 10)
        print(value_generate)
        if(value_generate>=4 and value_generate<=6):
            raise ValueError("Number generated is between 4 and 6")
        time.sleep(1)
        nr_of_nrs_generated+=1
