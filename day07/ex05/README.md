what are the differences
between OLAP and OLTP

OLTP and OLAP both are the online processing systems. OLTP is a transactional processing while OLAP is an analytical processing system. OLTP is a system that manages transaction-oriented applications on the internet for example, ATM. OLAP is an online system that reports to multidimensional analytical queries like financial reporting, forecasting, etc.

The basic difference between OLTP and OLAP is that OLTP is an online database modifying system, whereas, OLAP is an online database query answering system.

1.OLTP -It is an online transactional system and manages database modification.
  OLAP -It is an online data retrieving and data analysis system.
2.OLTP -	Insert, Update, Delete information from the database.
  OLAP -Extract data for analyzing that helps in decision making.
3 OLTP - OLTP and its transactions are the original source of data.
  OLAP - Different OLTPs database becomes the source of data for OLAP.
4 OLTP - OLTP has short transactions.
  OLAP - OLAP has long transactions.
5 OLTP - 	The processing time of a transaction is comparatively less in OLTP.
  OLAP - The processing time of a transaction is comparatively more in OLAP.The processing time of a transaction is comparatively more in OLAP.
6 OLTP - Simpler queries. 
  OLAP - Complex queries.
7 OLTP - Tables in OLTP database are normalized (3NF).
  OLAP - Tables in OLAP database are not normalized.
8 OLTP - OLTP database must maintain data integrity constraint.
  OLAP - OLAP database does not get frequently modified. Hence, data integrity is not affected.
