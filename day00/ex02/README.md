Un soft link consta dintr-un tip special de fiser care serveste ca o referinta unui alt fiser sau director.
Folosim comanda ln -s {source-dir-name} {symbolic-dir-name} pentru a creea linkuri simbolice.Un hard link este un fisier care indica acelasi inode subadiacent, ca la un alt fisier.In cazul in care stergem un fiser,acesta elimina un link catre inode-ul de baza
