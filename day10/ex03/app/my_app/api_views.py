import flask
from my_app import my_app
from google.cloud import pubsub_v1

class Publish:
    def __init__(self):
        # TODO(developer)
        self.project_id = "cosmic-zenith-327309"
        self.topic_id = "Fully-automated"
        self.publisher = pubsub_v1.PublisherClient()
        self.topic_path = self.publisher.topic_path(self.project_id, self.topic_id)
    def run(self,data):
        # Data must be a bytestring
        data = data.encode("utf-8")
        # Add two attributes, origin and username, to the message
        future = self.publisher.publish(
            self.topic_path, data, origin="python-sample", username="gcp"
        )
        print(future.result())




@my_app.route("/publish",methods=["POST"])
def publish_message():
    message = flask.request.form['publish_message']
    print(message)
    publish = Publish()
    publish.run(message)
    return "Mesajul a fost trimis cu succes!",201
