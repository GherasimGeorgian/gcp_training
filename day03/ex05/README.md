Firewall rules define what kind of Internet traffic is allowed or blocked. ... A firewall rule can be applied to traffic from the Internet to your computer (inbound), or from your computer to the Internet (outbound). A rule can also be applied to both directions at the same time.
For example:
-Determine what traffic your firewall allows and what is blocked.
-Examine the control information in individual packets, and either block or allow them according to the criteria that you define.
-Control how the firewalls protect your network from malicious programs and unauthorized access.

Common examples of firewall rules
Allow SSH traffic
Allow HTTP traffic
Multiple web and database server setup
Bastion host setup
