The basic structures of a relational database (as defined by the relational model) are tables, columns (or fields), rows (or records), and keys.
