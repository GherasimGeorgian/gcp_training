A dictionary is a collection data type that is unordered and indexed and can be changed and uses curly braces. It uses keys and values for the structure of the data. The key is the input of the data and the value is the actual data you are using. 
A list is a collection where you can put your values using square brackets, which is ordered and changeable. You can put several data-types in a list, such as integers, strings, and booleans.
A tuple is a collection data type that is ordered and not changeable. It uses parentheses where you can put in the values.
