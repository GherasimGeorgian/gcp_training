from google.cloud import bigquery



def query_stackoverflow():
    client = bigquery.Client()
    query_job = client.query(
        "SELECT score,text,creation_date FROM `bigquery-public-data.stackoverflow.comments` LIMIT 100"
    )

    results = query_job.result()  # Waits for job to complete.

    for row in results:
        var_cd = row.text
        if len(var_cd) > 120:
            var_cd=row.text[0:119]
        print("Score: {:<5}  Comment: {:<120} Creation date: {:<10}".format(row.score, var_cd,str(row.creation_date)))


if __name__ == "__main__":
    query_stackoverflow()
