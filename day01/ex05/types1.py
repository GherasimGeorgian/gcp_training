def create_types():
    
    try:
        f=open("demo.txt", "r")
    except IOError:
        raise IOError("Invalid input!")
    Lines = f.readlines()
    if(len(Lines)==0):
        raise ValueError("Invalid input!")
    numbers_list=[]
    for line in Lines:
        numbers_aux = line.strip().split(',')
        
        for el in numbers_aux:
            try:
                numbers_list.append(int(el))
            except ValueError:
                raise ValueError("Invalid input")
    dictionary = {i : numbers_list[i] for i in range(0, len(numbers_list))}   
    print(numbers_list,tuple(numbers_list),dictionary)
    return numbers_list,tuple(numbers_list),dictionary
