from flask import Flask, render_template
app = Flask(__name__,template_folder='app/templates/public',static_folder="app/static/img")
@app.route('/')
def homePage():
    return render_template("homepage.html")
@app.route('/contact')
def contactPage():
    return render_template("contact.html")
@app.route('/prices')
def pricesPage():
    return render_template("prices.html")

if __name__=="__main__":
    app.run(debug=True)