from flask import render_template
from __init__ import routes
from flask import request
import requests

@routes.route('/vaccinated-population')
def vacinated_population():
    return render_template("index.html")

@routes.route('/vaccinated-population',methods=['POST'])
def procent_covid():
    text = request.form['text']
    #{'error': {'code': 'https_access_restricted', 'message': 'Access Restricted - Your current Subscription Plan does not support HTTPS Encryption.'}}
    #
    #country = requests.get("http://api.worldbank.org/v2/country/br?format=json")
    #
    # 
    vacinated_population = requests.get("https://covid-api.mmediagroup.fr/v1/vaccines?country="+text)

    #procent = int(vacinated_population.json()['population'])/int(vacinated_population.json()['people_vaccinated'])
    procent = ((vacinated_population.json()['All'])['people_vaccinated']/(vacinated_population.json()['All'])['population'])*100
    return "Procent :" + str(procent) + "%"




@routes.route('/')
def homePage():
    return render_template("homepage.html")
@routes.route('/contact')
def contactPage():
    return render_template("contact.html")
@routes.route('/prices')
def pricesPage():
    return render_template("prices.html")
