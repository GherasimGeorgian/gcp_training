def count_words():
    
    sentence = input("Enter a sentence: ")
    if(len(sentence)==0):
        raise EOFError("no input provided!")
    words_list=sentence.split()
    number_of_words=len(words_list)
    return number_of_words
