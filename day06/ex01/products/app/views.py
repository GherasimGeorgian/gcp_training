from flask import render_template
from __init__ import routes


@routes.route('/')
def homePage():
    return render_template("homepage.html")
@routes.route('/contact')
def contactPage():
    return render_template("contact.html")
@routes.route('/prices')
def pricesPage():
    return render_template("prices.html")