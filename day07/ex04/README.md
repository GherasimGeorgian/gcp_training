what are the differences
between Big Query and Datastore

Datastore is designed to support transactional workloads, such as the backend for a web application. It's optimized for small transactions that read or write a limited number of rows per operation, with strong consistency guarantees. BigQuery is designed for analytic workloads.

A transactional or OLTP (online transaction processing) workload is a workload typically identified by a database receiving both requests for data and multiple changes to this data from a number of users over time where these modifications are called transactions.

An analytics workload is a set of queries that read and write datasets; it consumes compute, storage and memory resources and has an expected level of availability, robustness and performance. 
