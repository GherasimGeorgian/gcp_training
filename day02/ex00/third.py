def raise_to_three(src_list):
    new_list = [i ** 3 for i in src_list]
    return new_list
