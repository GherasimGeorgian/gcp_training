import threading
import multiprocessing
import random
import time
def thread_function(dur):
    time.sleep(120)




def worker(name: str) -> None:
    print(f'Started worker {name}')
    
    threads = list()

    for index in range(1,4):
        x = threading.Thread(target=thread_function, args=(index,))
        threads.append(x)
        x.start()
    for index, thread in enumerate(threads):
        thread.join()

    
    
    print(f'{name} worker finished')
if __name__ == '__main__':
    processes = []
    for i in range(5):
        process = multiprocessing.Process(target=worker, args=(f'process_{i}',))
        processes.append(process)
        process.start()
    for proc in processes:
        proc.join()
