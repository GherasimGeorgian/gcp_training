from google.cloud import bigquery
import csv
import json
from collections import defaultdict

def query_stackoverflow():
    client = bigquery.Client()
    query_job = client.query(
        "SELECT location,avg(reputation) as avgr ,display_name from `bigquery-public-data.stackoverflow.users`  group by location,reputation,display_name  order by avg(reputation) desc LIMIT 10"
        )

    results = query_job.result()  # Waits for job to complete.
    
    dictionary = {}
    
    locations_dict = defaultdict(list)
    for row in results:
            locations_dict[row.location].append(row.display_name)
            dictionary[row.location]={"average_value":row.avgr,"user_list":locations_dict[row.location]}
    print(locations_dict)
    #for row in results:
    #    dictionary[row.location]={"average_value":row.avgr,"user_list":users_list}
    with open('result.json','w') as f:
        f.write(json.dumps(dictionary))
if __name__ == "__main__":
    query_stackoverflow()

