#!/bin/bash
 
# PIDs of processes runned longer then 3 seconds
PIDS="${PIDS} `ps -eo comm,pid,etimes | awk '/^(python3) / {if ($3 >= 3) {print $2}}'`"
echo $PIDS
# Kill the processes
for i in ${PIDS}; do { echo "Killing $i"; kill -9 $i; }; done;
