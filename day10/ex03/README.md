The Microservice Architecture contains fine-grained services and lightweight protocols. Let's take an example of e-commerce application developed with microservice architecture. In this Microservices architecture example, each microservice is focused on single business capability.

